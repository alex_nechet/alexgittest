package com.gittest.alex.alexgittest.issues;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ListView;
import android.widget.TextView;

import com.gittest.alex.alexgittest.R;
import com.gittest.alex.alexgittest.common.model.Label;
import com.gittest.alex.alexgittest.issues.model.IssuesListResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class IssuesListAdapter extends RecyclerView.Adapter<IssuesListAdapter.Holder>
        implements Filterable{

    class Holder extends RecyclerView.ViewHolder{

        @BindView(R.id.issue_name_textView)
        TextView issueName;

        @BindView(R.id.issue_description_textView)
        TextView issueDescription;

        @BindView(R.id.labels_list)
        ListView lableList;

        Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private List<IssuesListResponse> issuesList = new ArrayList<>();
    private List<IssuesListResponse> mData = new ArrayList<>();
    private Context context;
    private ValueFilter valueFilter;

    public IssuesListAdapter(List<IssuesListResponse> issuesList, Context context) {
        mData = issuesList;
        this.issuesList = issuesList;
        this.context = context;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_issue, parent, false);
        return new Holder(v);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        IssuesListResponse issue = issuesList.get(position);
        String title = issue.getTitle();
        String description = issue.getBody();
        List<Label> labels = issue.getLabels();
        LabelAdapter labelAdapter = new LabelAdapter(R.layout.layout_label, labels, context);

        holder.lableList.setAdapter(labelAdapter);
        holder.issueName.setText(title);
        holder.issueDescription.setText(description);
    }

    @Override
    public int getItemCount() {
        return issuesList.size();
    }

    @Override
    public Filter getFilter() {
        if (valueFilter == null) {
            valueFilter = new ValueFilter();
        }
        return valueFilter;
    }

    private class ValueFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (constraint != null && constraint.length() > 0) {
                List<IssuesListResponse> filterList = new ArrayList<>();
                for (int i = 0; i < mData.size(); i++) {
                    String title = mData.get(i).getTitle();
                    if ((title.toUpperCase()).contains(constraint.toString().toUpperCase())) {
                        filterList.add(mData.get(i));
                    }
                }
                results.count = filterList.size();
                results.values = filterList;
            } else {
                results.count = mData.size();
                results.values = mData;
            }
            return results;

        }

        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            issuesList = (List) results.values;
            notifyDataSetChanged();
        }

    }

}
