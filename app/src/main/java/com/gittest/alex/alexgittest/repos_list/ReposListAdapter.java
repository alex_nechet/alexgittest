package com.gittest.alex.alexgittest.repos_list;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gittest.alex.alexgittest.R;
import com.gittest.alex.alexgittest.repos_list.model.ReposListResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReposListAdapter extends RecyclerView.Adapter<ReposListAdapter.Holder> {

    class Holder extends RecyclerView.ViewHolder{

        @BindView(R.id.card_repo)
        CardView cardView;

        @BindView(R.id.permission_TextView)
        TextView permissionTextView;

        @BindView(R.id.repo_name_textView)
        TextView repoNameTextView;

        @BindView(R.id.repo_description_textView)
        TextView repoDescriptionTextView;

        @BindView(R.id.owner_icon_imageView)
        ImageView ownerImageView;

        @BindView(R.id.owner_name_textView)
        TextView ownerNameTextView;


        Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private List<ReposListResponse> reposList = new ArrayList<>();
    private Context context;
    private ReposListAdapterListener listener;

    public ReposListAdapter(List<ReposListResponse> reposList, Context context, ReposListAdapterListener listener) {
        this.reposList = reposList;
        this.context = context;
        this.listener = listener;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_repo, parent, false);
        return new Holder(v);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        ReposListResponse repo = reposList.get(position);
        String repoName = repo.getName();
        String ownerName = repo.getOwner().getLogin();
        Boolean isPrivate = repo.getPrivate();
        String repoDescription = repo.getDescription();
        String ownerPhotoLink = repo.getOwner().getAvatarUrl();

        holder.repoNameTextView.setText(repoName);
        holder.repoDescriptionTextView.setText(repoDescription);
        holder.ownerNameTextView.setText(ownerName);

        Glide.with(context)
                .load(ownerPhotoLink)
                .into(holder.ownerImageView);

        holder.permissionTextView.setText(isPrivate ? context.getString(R.string.private_): context.getString(R.string.public_));
        holder.cardView.setOnClickListener(view -> listener.onCardClick(ownerName, repoName));

    }

    @Override
    public int getItemCount() {
        return reposList.size();
    }
}
