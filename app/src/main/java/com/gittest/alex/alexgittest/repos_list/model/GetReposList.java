package com.gittest.alex.alexgittest.repos_list.model;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by alex on 10/6/17.
 */

public interface GetReposList {

    @GET("user/repos")
    Observable<List<ReposListResponse>> getReposList (@Query("type") String type,
                                                      @Query("page") Integer page,
                                                      @Query("per_page") Integer perPage,
                                                      @Query("sort") String sort);
}
