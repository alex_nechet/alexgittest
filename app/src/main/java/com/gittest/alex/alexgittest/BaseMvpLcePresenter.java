package com.gittest.alex.alexgittest;

public interface BaseMvpLcePresenter {
    void handleError(Throwable throwable, boolean pullToRefresh);
}