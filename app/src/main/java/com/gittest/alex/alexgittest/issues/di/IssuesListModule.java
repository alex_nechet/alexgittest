package com.gittest.alex.alexgittest.issues.di;


import android.support.annotation.NonNull;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class IssuesListModule {

    @Provides
    @NonNull
    @Singleton
    public IssuesListProvider provideIssuesList(){
        return new IssuesListProvider();
    }
}
