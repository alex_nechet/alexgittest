package com.gittest.alex.alexgittest;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

public class AlertDialogFragment extends DialogFragment {

    private static final String MESSAGE = "message";
    private static final String TITLE = "title";
    private static final String ICON = "icon";

    public static AlertDialogFragment newInstance(int icon, String title, String message) {
        AlertDialogFragment frag = new AlertDialogFragment();
        Bundle args = new Bundle();
        args.putInt(ICON, icon);
        args.putString(TITLE, title);
        args.putString(MESSAGE, message);
        frag.setArguments(args);
        return frag;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        int icon = getArguments().getInt(ICON);
        String title = getArguments().getString(TITLE);
        String message = getArguments().getString(MESSAGE);

        return new AlertDialog.Builder(getActivity())
                .setIcon(icon)
                .setMessage(message)
                .setTitle(title)
                .setPositiveButton(R.string.ok, (dialogInterface, i) -> dialogInterface.dismiss())
                .create();
    }

}
