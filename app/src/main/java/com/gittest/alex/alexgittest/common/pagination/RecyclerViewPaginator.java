package com.gittest.alex.alexgittest.common.pagination;


import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.gittest.alex.alexgittest.common.pagination.AddNextItemsListener;
import com.gittest.alex.alexgittest.common.pagination.EndlessRecyclerViewScrollListener;

public class RecyclerViewPaginator {
    private final LinearLayoutManager linearLayoutManager;
    private final RecyclerView recyclerView;
    private final AddNextItemsListener addNextItemsListener;
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;

    public RecyclerViewPaginator(RecyclerView recyclerView,
                                 LinearLayoutManager linearLayoutManager,
                                 AddNextItemsListener addNextItemsListener) {
        this.linearLayoutManager = linearLayoutManager;
        this.recyclerView = recyclerView;
        this.addNextItemsListener = addNextItemsListener;
        paginate();
    }

    private void paginate() {
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                addNextItemsListener.onLoadNextItems(page);
                view.post(() -> addNextItemsListener.onInsertNextItems(page, totalItemsCount));
            }
        };
            recyclerView.addOnScrollListener(endlessRecyclerViewScrollListener);
        }

        public void resetPaginator(){
            endlessRecyclerViewScrollListener.resetState();
        }
}
