package com.gittest.alex.alexgittest.repos_list;

import com.gittest.alex.alexgittest.BaseRecyclerMvpLcePresenter;
import com.gittest.alex.alexgittest.Utils;
import com.gittest.alex.alexgittest.repos_list.di.DaggerMainActivityComponent;
import com.gittest.alex.alexgittest.repos_list.di.GetReposListModule;
import com.gittest.alex.alexgittest.repos_list.di.MainActivityComponent;
import com.gittest.alex.alexgittest.repos_list.di.ReposListProvider;
import com.gittest.alex.alexgittest.repos_list.model.ReposListResponse;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;

import retrofit2.HttpException;

public class ReposListActivityPresenter extends BaseRecyclerMvpLcePresenter<ReposListActivityView> {

    @Inject
    ReposListProvider reposListProvider;

    MainActivityComponent buildComponent() {
        return DaggerMainActivityComponent.builder()
                .getReposListModule(new GetReposListModule())
                .build();
    }

    public void requestReposList(String type, Integer page, Integer perPage, String sort,
                                 final boolean pullToRefresh){
        if (isViewAttached()){
            getView().showLoading(pullToRefresh);
            reposListProvider.getReposListResponse(type, page, perPage, sort)
                    .subscribe(this::handleResponse, error -> handleError(error, pullToRefresh));
        }
    }

    private void handleResponse(List<ReposListResponse> reposListResponses) {
        if (isViewAttached()) {
            getView().showContent();
            getView().setData(reposListResponses);
        }
    }

    @Override
    public void handleError(Throwable throwable, final boolean pullToRefresh) {
        if (isViewAttached()) {
            getView().showContent();

            if (throwable instanceof HttpException) {
                String errorResponse = Utils.parseError(throwable);
                if (errorResponse != null) {
                    if (isViewAttached()) {
                        getView().setResponseError(errorResponse);
                    }
                }
            }

            if (throwable instanceof IOException) {
                getView().showError(throwable, pullToRefresh);
            }
        }
    }
}
