package com.gittest.alex.alexgittest.issues.model;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface IssuesList {
    @GET("repos/{owner}/{repo}/issues")
    Observable<List<IssuesListResponse>> getIssuesList (@Path("owner") String owner,
                                                        @Path("repo") String repo);
}
