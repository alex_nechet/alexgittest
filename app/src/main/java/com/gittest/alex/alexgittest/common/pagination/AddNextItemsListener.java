package com.gittest.alex.alexgittest.common.pagination;

public interface AddNextItemsListener {
        void onLoadNextItems(int offset);
        void onInsertNextItems(int page, int totalItemsCount);
}
