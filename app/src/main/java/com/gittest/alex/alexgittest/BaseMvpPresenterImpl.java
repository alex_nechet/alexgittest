package com.gittest.alex.alexgittest;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;

import io.reactivex.disposables.Disposable;
import io.reactivex.disposables.Disposables;

public abstract class BaseMvpPresenterImpl<V extends MvpView> extends MvpBasePresenter<V>
implements BaseMvpPresenter{

    protected Disposable response = Disposables.empty();

    public void destroy(){
        response.dispose();
    }

}