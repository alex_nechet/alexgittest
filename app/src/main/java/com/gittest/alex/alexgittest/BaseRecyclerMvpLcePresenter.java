package com.gittest.alex.alexgittest;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

import io.reactivex.disposables.Disposable;
import io.reactivex.disposables.Disposables;

public abstract  class BaseRecyclerMvpLcePresenter<V extends MvpLceView> extends MvpBasePresenter<V>
implements BaseMvpLcePresenter{

    protected Disposable response = Disposables.empty();

    public void destroy(){
        response.dispose();
    }
}
