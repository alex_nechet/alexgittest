package com.gittest.alex.alexgittest.repos_list.di;

import com.gittest.alex.alexgittest.RetrofitServiceProvider;
import com.gittest.alex.alexgittest.repos_list.model.GetReposList;
import com.gittest.alex.alexgittest.repos_list.model.ReposListResponse;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class ReposListProvider {

    public Observable<List<ReposListResponse>> getReposListResponse(String type,
                                                                    Integer page,
                                                                    Integer perPage,
                                                                    String sort) {
        GetReposList  getReposList = RetrofitServiceProvider.builder.create(GetReposList.class);
        return getReposList.getReposList(type, page, perPage, sort)
                .retry(3)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}

