package com.gittest.alex.alexgittest.login;

import android.content.Context;
import android.webkit.WebView;

import com.gittest.alex.alexgittest.BaseMvpPresenterImpl;
import com.gittest.alex.alexgittest.Utils;

import java.io.IOException;

import retrofit2.HttpException;


public class LoginPresenter extends BaseMvpPresenterImpl<LoginView> {

    public static final String TAG = LoginPresenter.class.getSimpleName();

    private WebClientProvider webClientProvider = new WebClientProvider(token -> {
        if (isViewAttached()){
            getView().saveToken(token);
            getView().startMainActivity();
        }
    });

    public void login(WebView webView, String scope, Context context) {
            webClientProvider.getToken(webView, scope, context);
    }

    @Override
    public void handleError(Throwable throwable) {
        if (throwable instanceof HttpException) {
            String errorResponse = Utils.parseError(throwable);
            if (errorResponse != null) {
                if (isViewAttached()) {
                    getView().setResponseError(errorResponse);
                }
            }
        }

        if (throwable instanceof IOException) {
            if (isViewAttached()) {
                getView().setResponseError(throwable.getLocalizedMessage());
            }
        }
    }

}
