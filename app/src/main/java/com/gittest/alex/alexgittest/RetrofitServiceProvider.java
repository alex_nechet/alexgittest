package com.gittest.alex.alexgittest;


import android.util.Log;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.ResponseBody;

import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

import static okhttp3.logging.HttpLoggingInterceptor.Level.BODY;

public class RetrofitServiceProvider {

    private static final String TAG = RetrofitServiceProvider.class.getSimpleName();
    private static final String AUTHORIZATION = "Authorization";
    private static final String BEARER = "Bearer ";

    public static Retrofit builder = new Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .client(createOkHttpClient())
            .build();

    private static OkHttpClient createOkHttpClient() {
        // Add logging
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(BODY);

        String access = LocalStorage.readString(LocalStorage.ACCESS_TOKEN, "", App.getInstance());

        if (!access.equals("")) {

            return new OkHttpClient.Builder()
                    .addInterceptor(logging)
                    .addInterceptor(chain -> {
                        Request original = chain.request();
                        Request newRequest;
                        switch (original.method()) {

                            case "POST":
                                Log.d(TAG, "Adding post headers");
                                newRequest = original.newBuilder()
                                        .addHeader(AUTHORIZATION, BEARER + access)
                                        .build();
                                return chain.proceed(newRequest);

                            case "GET":
                                Log.d(TAG, "Adding get headers");
                                newRequest = original.newBuilder()
                                        .addHeader(AUTHORIZATION, BEARER + access)
                                        .build();
                                return chain.proceed(newRequest);

                            case "DELETE":
                                Log.d(TAG, "Adding delete headers");
                                newRequest = original.newBuilder()
                                        .addHeader(AUTHORIZATION, BEARER + access)
                                        .build();
                                return chain.proceed(newRequest);

                            case "PUT":
                                Log.d(TAG, "Adding put headers");
                                newRequest = original.newBuilder()
                                        .addHeader(AUTHORIZATION, BEARER + access)
                                        .build();
                                return chain.proceed(newRequest);

                            case "PATCH":
                                Log.d(TAG, "Adding patch headers");
                                newRequest = original.newBuilder()
                                        .addHeader(AUTHORIZATION, BEARER + access)
                                        .build();
                                return chain.proceed(newRequest);
                        }
                        return chain.proceed(original);
                    }).build();
        } else {
            return new OkHttpClient.Builder()
                    .addInterceptor(logging)
                    .build();
        }
    }
    public static <T> T parseResponseBody(Class<T> responseClass, ResponseBody response) {
        try {
            Converter<ResponseBody, T> errorConverter
                    = builder.responseBodyConverter(responseClass, new Annotation[0]);
            return errorConverter.convert(response);

        } catch (IOException e) {
            e.printStackTrace();
            Log.d(TAG, e.getLocalizedMessage());
        }
        return null;
    }
}
