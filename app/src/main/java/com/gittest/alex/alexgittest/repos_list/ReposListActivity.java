package com.gittest.alex.alexgittest.repos_list;

import android.content.Intent;
import android.os.Bundle;
import android.os.Process;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;


import com.gittest.alex.alexgittest.BaseRecyclerMvpLceActivity;
import com.gittest.alex.alexgittest.R;
import com.gittest.alex.alexgittest.Utils;
import com.gittest.alex.alexgittest.common.pagination.AddNextItemsListener;
import com.gittest.alex.alexgittest.common.pagination.RecyclerViewPaginator;
import com.gittest.alex.alexgittest.issues.IssuesActivity;
import com.gittest.alex.alexgittest.repos_list.model.ReposListResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReposListActivity extends BaseRecyclerMvpLceActivity<List<ReposListResponse>, ReposListActivityView, ReposListActivityPresenter>
        implements ReposListActivityView, AddNextItemsListener {

    public static final int PER_PAGE = 30;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    private ReposListAdapter adapter;
    private List<ReposListResponse> reposList = new ArrayList<>();
    private int pageCount = 1;
    private RecyclerViewPaginator paginator;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repos_list);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null)
                Utils.setDefaultActionbar(getSupportActionBar());

        presenter.buildComponent().inject(presenter);
        contentView.setOnRefreshListener(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        paginator = new RecyclerViewPaginator(recyclerView, linearLayoutManager, this);
        loadData(false);
    }

    @NonNull
    @Override
    public ReposListActivityPresenter createPresenter() {
        return new ReposListActivityPresenter();
    }


    @Override
    public void setResponseError(String errorMessage) {
        Utils.showDialogFragment(R.drawable.ic_error_red_24dp, getString(R.string.error), errorMessage, this);
    }

    @Override
    public void setData(List<ReposListResponse> data) {
        reposList.addAll(data);
        if (pageCount == 1) {
            adapter = new ReposListAdapter(reposList, this, (owner, repo) -> {
                Intent intent = new Intent(ReposListActivity.this, IssuesActivity.class);
                intent.putExtra(IssuesActivity.OWNER, owner);
                intent.putExtra(IssuesActivity.REPO, repo);
                startActivity(intent);
            });
            recyclerView.setAdapter(adapter);
        } else {
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void loadData(boolean pullToRefresh) {
        presenter.requestReposList("", pageCount, PER_PAGE, "", pullToRefresh);
    }

    @Override
    public void onDestroy() {
        presenter.destroy();
        super.onDestroy();
    }

    @Override
    public void onRefresh() {
        reposList.clear();
        pageCount = 1;
        paginator.resetPaginator();
        super.onRefresh();
    }

    @Override
    public void onLoadNextItems(int offset) {
        presenter.requestReposList("", offset, PER_PAGE, "", false);
        pageCount++;
    }

    @Override
    public void onInsertNextItems(int page, int totalItemsCount) {
        adapter.notifyItemRangeInserted(page, totalItemsCount);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                Snackbar.make(toolbar, R.string.ok_to_quit, Snackbar.LENGTH_LONG)
                        .setAction(R.string.ok, view -> Process.killProcess(Process.myPid()))
                        .show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}