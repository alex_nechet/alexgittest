package com.gittest.alex.alexgittest.repos_list.di;

import android.support.annotation.NonNull;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class GetReposListModule {

    @Provides
    @NonNull
    @Singleton
    public ReposListProvider provideReposList(){
        return new ReposListProvider();
    }

}

