package com.gittest.alex.alexgittest.issues;

import com.gittest.alex.alexgittest.BaseMvpLceView;
import com.gittest.alex.alexgittest.issues.model.IssuesListResponse;

import java.util.List;

/**
 * Created by alex on 10/6/17.
 */

public interface IssuesActivityView extends BaseMvpLceView<List<IssuesListResponse>> {
}
