package com.gittest.alex.alexgittest.login;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;


public class LoginTask extends AsyncTask<Void, Void, String> {

    private static final String TAG = LoginTask.class.getSimpleName();

    private static final String OAUTH_ACCESS_TOKEN_URL = "https://github.com/login/oauth/access_token";
    private static final String TOKEN = "access_token";

    private JSONObject params;
    private AsyncResponse asyncResponse;

    public LoginTask(JSONObject params, AsyncResponse asyncResponse) {
        this.params = params;
        this.asyncResponse = asyncResponse;
    }

    public interface AsyncResponse {
        void processFinish(String token);
    }

    @Override
    protected String doInBackground(Void... p) {
        try {
            String data;
            URL url = new URL(OAUTH_ACCESS_TOKEN_URL);
            String body = params.toString();
            byte[] bytes = body.getBytes();
            HttpURLConnection httpURLConnection = null;
            try {
                httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setUseCaches(false);
                httpURLConnection.setFixedLengthStreamingMode(bytes.length);
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setRequestProperty("Content-Type", "application/json");
                httpURLConnection.setRequestProperty("Accept", "application/json");
                httpURLConnection.setRequestProperty("User-Agent", "AlexGitTest");

                OutputStream out = httpURLConnection.getOutputStream();
                out.write(bytes);
                out.close();

                InputStream inputStream;
                try {
                    inputStream = httpURLConnection.getInputStream();
                }
                catch (IOException e) {
                    e.printStackTrace();
                    inputStream = httpURLConnection.getErrorStream();
                }

                BufferedReader rd = new BufferedReader(new InputStreamReader(inputStream));
                StringBuffer response = new StringBuffer();
                String line;

                while((line = rd.readLine()) != null) {
                    response.append(line).append("\n");
                }

                rd.close();
                data = response.toString();

            } finally {
                if (httpURLConnection != null) {
                    httpURLConnection.disconnect();
                }
            }

            try {
                JSONObject json = new JSONObject(data);
                return json.getString(TOKEN);


            } catch (JSONException e) {
                Log.e(TAG, e.getMessage());

            }
        }
        catch (IOException e) {
            Log.e(TAG, e.getMessage());

        }

        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        asyncResponse.processFinish(s);
    }
}
