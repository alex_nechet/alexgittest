package com.gittest.alex.alexgittest.login;
import com.hannesdorfmann.mosby3.mvp.MvpView;

public interface LoginView extends MvpView {
 void setResponseError(String error);
 void startMainActivity();
 void saveToken(String token);
}
