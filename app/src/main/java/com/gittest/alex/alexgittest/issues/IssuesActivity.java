package com.gittest.alex.alexgittest.issues;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.gittest.alex.alexgittest.BaseRecyclerMvpLceActivity;
import com.gittest.alex.alexgittest.R;
import com.gittest.alex.alexgittest.Utils;
import com.gittest.alex.alexgittest.issues.model.IssuesListResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class IssuesActivity extends BaseRecyclerMvpLceActivity<List<IssuesListResponse>, IssuesActivityView, IssuesActivityPresenter>
implements IssuesActivityView, SearchView.OnQueryTextListener{

    public static final String OWNER = "owner";
    public static final String REPO = "repo";

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    private SearchView searchView;
    private String owner;
    private String repo;
    private List<IssuesListResponse> responseList = new ArrayList<>();
    private IssuesListAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repos_list);
        ButterKnife.bind(this);
        presenter.buildComponent().inject(presenter);
        Intent intent = getIntent();

        if (intent != null){
            owner = intent.getStringExtra(OWNER);
            repo = intent.getStringExtra(REPO);
        }

        setSupportActionBar(toolbar);
        recyclerView.setLayoutManager(linearLayoutManager);

        if (getSupportActionBar() != null)
            Utils.setDefaultActionbar(getSupportActionBar());

        contentView.setOnRefreshListener(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        loadData(false);
    }

    @Override
    public void onDestroy() {
        presenter.destroy();
        super.onDestroy();
    }

    @NonNull
    @Override
    public IssuesActivityPresenter createPresenter() {
        return new IssuesActivityPresenter();
    }

    @Override
    public void setResponseError(String errorMessage) {
        Utils.showDialogFragment(R.drawable.ic_error_red_24dp, getString(R.string.error), errorMessage, this);
    }

    @Override
    public void setData(List<IssuesListResponse> data) {
        //NOTE: Returns up to 100 results therefore no pagination implemented. Will add if needs to.
        responseList.clear();
        responseList.addAll(data);
        adapter = new IssuesListAdapter(data, this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void loadData(boolean pullToRefresh) {
        presenter.getIssues(owner, repo, pullToRefresh);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_issues_activity, menu);
        MenuItem searchItem = menu.findItem(R.id.menu_search);
        searchView = (SearchView) searchItem.getActionView();

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);

        if(searchManager != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        }

        searchView.setOnQueryTextListener(this);
        searchView.setIconifiedByDefault(false);

        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        adapter.getFilter().filter(newText);
        return false;
    }
}
