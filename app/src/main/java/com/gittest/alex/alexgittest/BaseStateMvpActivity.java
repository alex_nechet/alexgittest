package com.gittest.alex.alexgittest;

import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;

import com.hannesdorfmann.mosby3.mvp.MvpActivity;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;

import icepick.Icepick;
import icepick.State;

public abstract class BaseStateMvpActivity<V extends MvpView, P extends MvpPresenter<V>> extends MvpActivity<V, P> {

    private static final String TAG = BaseStateMvpActivity.class.getSimpleName();

    @State
    protected Parcelable state;

    public Parcelable getState() {
        return state;
    }

    public void setState(Parcelable state) {
        this.state = state;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Icepick.restoreInstanceState(this, savedInstanceState);
        Log.d(TAG, "state restored");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Icepick.saveInstanceState(this, outState);
        Log.d(TAG, "state saved");
    }
}
