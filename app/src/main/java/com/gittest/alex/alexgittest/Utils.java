package com.gittest.alex.alexgittest;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;

import com.gittest.alex.alexgittest.common.model.ErrorResponse;

import retrofit2.HttpException;
import retrofit2.Response;


public class Utils {

    public static String parseError(Throwable throwable) {
        HttpException exception = (HttpException) throwable;
        Response resp = exception.response();
        ErrorResponse errorResponse =
                RetrofitServiceProvider.parseResponseBody(ErrorResponse.class, resp.errorBody());
        if (errorResponse != null) {
            return errorResponse.getMessage();
        }
        return null;
    }

    public static void showDialogFragment(int icon, String title, String message, FragmentActivity activity) {
        FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction();
        Fragment prev = activity.getSupportFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            activity.getSupportFragmentManager().popBackStack();
        }
        ft.addToBackStack(null);

        android.support.v4.app.DialogFragment newFragment = AlertDialogFragment.newInstance(icon, title, message);
        newFragment.show(ft, "dialog");
    }

    public static ActionBar setDefaultActionbar(ActionBar actionBar) {
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        return actionBar;
    }

}
