package com.gittest.alex.alexgittest.issues.di;


import com.gittest.alex.alexgittest.RetrofitServiceProvider;
import com.gittest.alex.alexgittest.issues.model.IssuesList;
import com.gittest.alex.alexgittest.issues.model.IssuesListResponse;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class IssuesListProvider {

    public Observable<List<IssuesListResponse>> getissuesListResponse (String owner, String repo) {
        IssuesList issuesList = RetrofitServiceProvider.builder.create(IssuesList.class);
        return issuesList.getIssuesList(owner, repo)
                .retry(3)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
