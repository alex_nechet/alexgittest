package com.gittest.alex.alexgittest;

import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

public interface BaseMvpLceView<M> extends MvpLceView<M> {
    void setResponseError(String errorMessage);
}