package com.gittest.alex.alexgittest.issues.di;

import com.gittest.alex.alexgittest.issues.IssuesActivityPresenter;

import javax.inject.Singleton;

import dagger.Component;


@Component(modules = {IssuesListModule.class})
@Singleton
public interface IssuesComponent {
    void inject (IssuesActivityPresenter issuesActivityPresenter);
}
