package com.gittest.alex.alexgittest;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;


public class LocalStorage {

    public static final String ACCESS_TOKEN = "access";

    public static void saveString(String key, String value, Context context){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String readString(String key, String defaultValue, Context context){
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getString(key, defaultValue);
    }

}
