package com.gittest.alex.alexgittest;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.MenuItem;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceActivity;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

import icepick.Icepick;
import icepick.State;

public abstract class BaseRecyclerMvpLceActivity<M, V extends MvpLceView<M>, P extends MvpBasePresenter<V>>
        extends MvpLceActivity<SwipeRefreshLayout, M, V, P>
        implements SwipeRefreshLayout.OnRefreshListener {

    @State
    Parcelable listState;

    protected LinearLayoutManager linearLayoutManager;

    public Parcelable getListState() {
        return listState;
    }

    public void setListState(Parcelable listState) {
        this.listState = listState;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Icepick.restoreInstanceState(this, savedInstanceState);
        linearLayoutManager = new LinearLayoutManager(this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Icepick.saveInstanceState(this, outState);
        setListState(linearLayoutManager.onSaveInstanceState());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setData(M data) {}

    @Override
    public void onRefresh() {
        loadData(false);
    }

    @Override
    public void showLoading(boolean pullToRefresh) {
        super.showLoading(pullToRefresh);
        contentView.setRefreshing(pullToRefresh);
    }

    @Override
    protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return null;
    }

    @Override
    public void showError(Throwable e, boolean pullToRefresh) {
        super.showError(e, pullToRefresh);
        contentView.setRefreshing(false);
        Utils.showDialogFragment(R.drawable.ic_error_red_24dp, getString(R.string.error), e.getLocalizedMessage(), this);
    }

    @Override
    public void showContent() {
        super.showContent();
        contentView.setRefreshing(false);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView(false);
    }
}