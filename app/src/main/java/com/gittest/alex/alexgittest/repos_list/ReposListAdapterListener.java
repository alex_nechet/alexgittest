package com.gittest.alex.alexgittest.repos_list;

public interface ReposListAdapterListener {
    void onCardClick(String owner, String repo);
}
