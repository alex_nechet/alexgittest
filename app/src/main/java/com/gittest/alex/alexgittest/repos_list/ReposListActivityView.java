package com.gittest.alex.alexgittest.repos_list;


import com.gittest.alex.alexgittest.BaseMvpLceView;
import com.gittest.alex.alexgittest.repos_list.model.ReposListResponse;

import java.util.List;

public interface ReposListActivityView extends BaseMvpLceView<List<ReposListResponse>> {

}
