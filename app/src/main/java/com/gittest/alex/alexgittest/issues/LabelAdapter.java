package com.gittest.alex.alexgittest.issues;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gittest.alex.alexgittest.R;
import com.gittest.alex.alexgittest.common.model.Label;
import java.util.List;

/**
 * Created by alex on 10/6/17.
 */

public class LabelAdapter extends ArrayAdapter<Label> {

    private Context context;
    private List<Label> labelList;
    private int layout;

    public LabelAdapter( int layout, List<Label> labelList, Context context) {
        super(context, layout, labelList);
        this.context = context;
        this.labelList = labelList;
        this.layout = layout;
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LinearLayout rowView;
        if (convertView == null) {
            rowView = new LinearLayout(getContext());
            LayoutInflater layoutInflater = LayoutInflater.from(context);
            layoutInflater.inflate(layout, rowView, true);
        } else {
            rowView = (LinearLayout) convertView;
        }

        CardView cardView = (CardView)rowView.findViewById(R.id.chip);
        TextView textView = (TextView)rowView.findViewById(R.id.label_text);

        String color = labelList.get(position).getColor();
        String name = labelList.get(position).getName();

        cardView.setCardBackgroundColor(Color.parseColor("#" + color));
        textView.setText(name);
        return rowView;
    }
}
