package com.gittest.alex.alexgittest.repos_list.di;

import com.gittest.alex.alexgittest.repos_list.ReposListActivityPresenter;

import javax.inject.Singleton;

import dagger.Component;

@Component(modules = {GetReposListModule.class})
@Singleton
public interface MainActivityComponent {
    void inject (ReposListActivityPresenter reposListActivityPresenter);
}