package com.gittest.alex.alexgittest;

public interface BaseMvpPresenter {
    void handleError(Throwable throwable);
}
