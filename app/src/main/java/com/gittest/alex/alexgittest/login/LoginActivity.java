package com.gittest.alex.alexgittest.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.webkit.WebView;


import com.gittest.alex.alexgittest.LocalStorage;
import com.gittest.alex.alexgittest.R;
import com.gittest.alex.alexgittest.Utils;
import com.gittest.alex.alexgittest.repos_list.ReposListActivity;
import com.hannesdorfmann.mosby3.mvp.MvpActivity;


import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends MvpActivity<LoginView, LoginPresenter>
        implements LoginView {

    private static final String TAG = LoginActivity.class.getSimpleName();

    @BindView(R.id.webview)
    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!LocalStorage.readString(LocalStorage.ACCESS_TOKEN, "", this).equals("")){
            startMainActivity();
        }
        Log.d(TAG, "activity created");
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        presenter.login(webView, "", this);
        new WebClientProvider(token -> {
            if (token != null) {
                LocalStorage.saveString(LocalStorage.ACCESS_TOKEN, token, LoginActivity.this);
                startMainActivity();
            }
        });
    }


    @Override
    protected void onDestroy() {
        presenter.destroy();
        super.onDestroy();
    }

    @NonNull
    @Override
    public LoginPresenter createPresenter() {
        return new LoginPresenter();
    }

    @Override
    public void setResponseError(String localizedMessage) {
        Utils.showDialogFragment(R.drawable.ic_error_red_24dp, getString(R.string.error), localizedMessage, this);
    }

    @Override
    public void startMainActivity() {
        startActivity(new Intent(LoginActivity.this, ReposListActivity.class));
        finish();
    }

    @Override
    public void saveToken(String token) {
        LocalStorage.saveString(LocalStorage.ACCESS_TOKEN, token, this);
    }
}
