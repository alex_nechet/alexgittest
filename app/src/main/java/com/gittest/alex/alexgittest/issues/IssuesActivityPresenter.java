package com.gittest.alex.alexgittest.issues;

import com.gittest.alex.alexgittest.BaseRecyclerMvpLcePresenter;
import com.gittest.alex.alexgittest.Utils;
import com.gittest.alex.alexgittest.issues.di.DaggerIssuesComponent;
import com.gittest.alex.alexgittest.issues.di.IssuesComponent;
import com.gittest.alex.alexgittest.issues.di.IssuesListModule;
import com.gittest.alex.alexgittest.issues.di.IssuesListProvider;
import com.gittest.alex.alexgittest.issues.model.IssuesListResponse;

import java.io.IOException;
import java.util.List;

import javax.inject.Inject;

import retrofit2.HttpException;


public class IssuesActivityPresenter extends BaseRecyclerMvpLcePresenter<IssuesActivityView> {

    @Inject
    IssuesListProvider issuesListProvider;


    IssuesComponent buildComponent() {
        return DaggerIssuesComponent.builder()
                .issuesListModule( new IssuesListModule())
                .build();
    }

    public void getIssues(String owner,
                          String repo,
                          final boolean pullToRefresh){
        if (isViewAttached()) {
            getView().showLoading(pullToRefresh);
            issuesListProvider.getissuesListResponse(owner, repo)
                    .subscribe(this::handleResponse, error -> handleError(error, pullToRefresh));
        }
    }

    private void handleResponse(List<IssuesListResponse> issuesListResponses) {
        if (isViewAttached()) {
            getView().showContent();
            getView().setData(issuesListResponses);
        }
    }

    @Override
    public void handleError(Throwable throwable, final boolean pullToRefresh) {
        if (isViewAttached()) {
            getView().showContent();

            if (throwable instanceof HttpException) {
                String errorResponse = Utils.parseError(throwable);
                if (errorResponse != null) {
                    if (isViewAttached()) {
                        getView().setResponseError(errorResponse);
                    }
                }
            }

            if (throwable instanceof IOException) {
                getView().showError(throwable, pullToRefresh);
            }
        }
    }
}
