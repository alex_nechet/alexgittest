package com.gittest.alex.alexgittest.login;

import android.content.Context;
import android.graphics.Bitmap;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.gittest.alex.alexgittest.R;

import org.json.JSONException;
import org.json.JSONObject;

public class WebClientProvider {

    private static final String OAUTH_URL = "https://github.com/login/oauth/authorize";
    private static final String CLIENT_ID = "client_id";
    private static final String CLIENT_SECRET = "client_secret";
    private static final String CODE = "code";
    private TokenListener listener;


    public WebClientProvider(TokenListener listener) {
        this.listener = listener;
    }

    public interface TokenListener {
        void getTokenFromTask(String token);
    }

    public void getToken (WebView webView, String scope, Context context){
       String clientID = context.getString(R.string.client_id);
       String clientSecret = context.getString(R.string.client_secret);
       String url = OAUTH_URL + "?client_id=" + clientID;
       webView.getSettings().setJavaScriptEnabled(true);
       webView.loadUrl(url + "&scope=" + scope);
       webView.setWebViewClient(new WebViewClient() {
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                String fragment = "?code=";
                int start = url.indexOf(fragment);
                if (start > -1) {
                    webView.stopLoading();
                    final String code = url.substring(start+fragment.length(), url.length());
                    try {
                        JSONObject params = new JSONObject();
                        params.put(CLIENT_ID, clientID);
                        params.put(CLIENT_SECRET, clientSecret);
                        params.put(CODE, code);
                        new LoginTask(params, token -> listener.getTokenFromTask(token)).execute();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }
}
