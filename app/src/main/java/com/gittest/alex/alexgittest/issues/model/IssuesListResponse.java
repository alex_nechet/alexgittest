package com.gittest.alex.alexgittest.issues.model;

import com.gittest.alex.alexgittest.common.model.Assignee;
import com.gittest.alex.alexgittest.common.model.Label;
import com.gittest.alex.alexgittest.common.model.Milestone;
import com.gittest.alex.alexgittest.common.model.PullRequest;
import com.gittest.alex.alexgittest.common.model.User;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class IssuesListResponse {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("repository_url")
    @Expose
    private String repositoryUrl;
    @SerializedName("labels_url")
    @Expose
    private String labelsUrl;
    @SerializedName("comments_url")
    @Expose
    private String commentsUrl;
    @SerializedName("events_url")
    @Expose
    private String eventsUrl;
    @SerializedName("html_url")
    @Expose
    private String htmlUrl;
    @SerializedName("number")
    @Expose
    private Integer number;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("body")
    @Expose
    private String body;
    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("labels")
    @Expose
    private List<Label> labels = null;
    @SerializedName("assignee")
    @Expose
    private Assignee assignee;
    @SerializedName("milestone")
    @Expose
    private Milestone milestone;
    @SerializedName("locked")
    @Expose
    private Boolean locked;
    @SerializedName("comments")
    @Expose
    private Integer comments;
    @SerializedName("pull_request")
    @Expose
    private PullRequest pullRequest;
    @SerializedName("closed_at")
    @Expose
    private Object closedAt;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("assignees")
    @Expose
    private List<Assignee.Assignee_> assignees = null;

    public Integer getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }

    public String getRepositoryUrl() {
        return repositoryUrl;
    }

    public String getLabelsUrl() {
        return labelsUrl;
    }

    public String getCommentsUrl() {
        return commentsUrl;
    }

    public String getEventsUrl() {
        return eventsUrl;
    }

    public String getHtmlUrl() {
        return htmlUrl;
    }

    public Integer getNumber() {
        return number;
    }

    public String getState() {
        return state;
    }

    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }

    public User getUser() {
        return user;
    }

    public List<Label> getLabels() {
        return labels;
    }

    public Assignee getAssignee() {
        return assignee;
    }

    public Milestone getMilestone() {
        return milestone;
    }

    public Boolean getLocked() {
        return locked;
    }

    public Integer getComments() {
        return comments;
    }

    public PullRequest getPullRequest() {
        return pullRequest;
    }


    public Object getClosedAt() {
        return closedAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public List<Assignee.Assignee_> getAssignees() {
        return assignees;
    }

}












