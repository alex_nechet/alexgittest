package com.gittest.alex.alexgittest.common.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Permissions {

    @SerializedName("admin")
    @Expose
    private Boolean admin;
    @SerializedName("push")
    @Expose
    private Boolean push;
    @SerializedName("pull")
    @Expose
    private Boolean pull;

    public Boolean getAdmin() {
        return admin;
    }

    public Boolean getPush() {
        return push;
    }

    public Boolean getPull() {
        return pull;
    }

}
